# optimal plan for Problems 1, 2, and 3

### optimal plan for Problems 1

```
Load(C1, P1, SFO)
Load(C2, P2, JFK)
Fly(P1, SFO, JFK)
Fly(P2, JFK, SFO)
Unload(C1, P1, JFK)
Unload(C2, P2, SFO)
```


### optimal plan for Problems 1

```
Load(C1, P1, SFO)
Fly(P1, SFO, JFK)
Unload(C1, P1, JFK)
Load(C2, P2, JFK)
Fly(P2, JFK, SFO)
Unload(C2, P2, SFO)
Load(C3, P3, ATL)
Fly(P3, ATL, SFO)
Unload(C3, P3, SFO)
```


### optimal plan for Problems 1

```
Load(C1, P1, SFO)
Fly(P1, SFO, ATL)
Load(C3, P1, ATL)
Fly(P1, ATL, JFK)
Unload(C1, P1, JFK)
Load(C2, P2, JFK)
Fly(P2, JFK, ORD)
Load(C4, P2, ORD)
Fly(P2, ORD, SFO)
Unload(C2, P2, SFO)
Unload(C3, P1, JFK)
Unload(C4, P2, SFO)
```

# Compare and contrast non-heuristic search result metrics
| Problem        | search | heuristic  | Plan length  | Expansions | Time(sec) |
| ------------- |:-------------:| -----:| --- | - | - | - |
| 1      | breadth_first_search |  | 6 | 43 | 0.04871188666039528 |
| 1      | depth_first_graph_search |  | 20 | 21 | 0.014255086745978529 |
| 1      | uniform_cost_search |  | 6 | 55 | 0.05160980119619407 |
| 2      | breadth_first_search |  | 9 | 3343 | 22.757198895784427 |
| 2      | depth_first_graph_search |  | 619 | 624 | 6.037737234431852 |
| 2      | uniform_cost_search |  | 9 | 4852 | 20.87112353105814 |
| 3      | breadth_first_search |  | 12 | 14663 | 167.71358648603092 |
| 3      | depth_first_graph_search |  | 392 | 408 | 3.5851785941976004 |
| 3      | uniform_cost_search |  | 12 | 18150 | 87.4324465370099 |

###### breadth_first_search:
a simple strategy in which the root node is expanded ﬁrst, then all the
successors of the root node are expanded next, then their successors, and so on. It is completed but the news in space and time is not good.

###### depth_first_graph_search:
it always expands the deepest node in the current frontier of the search tree.The properties of depth-ﬁrst search depend strongly on whether the graph-search or tree-search version is used. It is completed when using graph-search but not completd when using tree-search.Depth_first_search is better than breadth_first_search in space.
###### uniform_cost_search:
It is completed sometimes and expands the node with the lowest path cost

In three search, breadth_first_search and uniform_cost_search get same optimal plann length but breadth_first_search need the smaller expansions and time. In three search, depth_first_graph_search gets the most small expansions and time but most plan length.

# Compare and contrast  heuristic search result metrics
| Problem        | search | heuristic  | Plan length  | Expansions | Time(sec) |
| ------------- |:-------------:| -----:| --- | - | - | - |
| 1      | astar_search | h_1  | 6 | 55 | 0.05478878304564067 |
| 1      | astar_search | h_ignore_preconditions | 6 | 41 | 0.053924264201695805 |
| 1      | astar_search | h_pg_levelsum   | 6 | 11 | 1.121283703898743   |
| 2      | astar_search | h_1  | 9 | 4852 | 15.46690965304372 |
| 2      | astar_search | h_ignore_preconditions | 9 | 1450 | 5.668076477152821 |
| 2      | astar_search | h_pg_levelsum   | 9 | 86 | 101.78562419642185 |
| 3      | astar_search | h_1  | 12 | 18150 | 77.33868532343071 |
| 3      | astar_search | h_ignore_preconditions | 12 | 5038 | 26.85859511345528 |

##### h_ignore_preconditions:
the ignore pre-conditions heuristic drops all preconditions from actions.
Every action becomes applicable in every state, and any single goal ﬂuent can be achieved in one step.
##### h_pg_levelsum:
The level sum heuristic, following the subgoal independence assumption, returns the sum of the level costs of the goals.

Three heuristics get same optimal plan length. In three heuristics, h_ignore_preconditions is the fatest and h_pg_levelsum consumes the smallest expansions but is the slowest.


# References
- Russell, S., & Norvig, P. (2010). Artificial Intelligence: a modern approach.
