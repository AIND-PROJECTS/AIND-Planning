Currently the most popular and effective approaches to fully automated planning are
- Translating to a Boolean satisﬁability (SAT) problem.
- Forward state-space search with carefully crafted heuristics.
- Search using a planning graph.

# Boolean satisfiability
The Boolean Satisfiability Problem (sometimes called Propositional Satisfiability Problem and abbreviated as SAT) is the problem of determining if there exists an interpretation that satisfies a given Boolean formula. SAT is the first problem that was proven to be NP-complete. Today's heuristical SAT-algorithms are able to solve problem instances involving tens of thousands of variables and formulas consisting of millions of symbols, which is sufficient for many practical SAT problem.
 # Forward search with heuristics
Until around 1998 it was assumed that forward state-space search was too inefﬁcient to be practical.
- First, forward search is prone to exploring irrelevant actions
- Second, planning problems often have large state spaces

It turns out  that strong domain-independent heuristics can be derived automatically; that is what makes forward search feasible.
# Planning graph
Planning graphs work only for propositional planning problems.
Before Graphplan came out, most planning researchers were working on PSP-like planners.Graphplan caused a sensation because it was so much faster.
Many subsequent planning systems have used ideas from it, such as:
IPP, STAN, GraphHTN, SGP, Blackbox, Medic, TGP, LPG.
Many of them are much faster than the original Graphplan

# References
- Russell, S., & Norvig, P. (2010). Artificial Intelligence: a modern approach.
- https://en.wikipedia.org/wiki/Heuristic_(computer_science).
- Jussi Rintanen and Jorg Hoffmann. An overview of recent algorithms for AI planning.
